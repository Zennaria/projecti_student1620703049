﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceShip enemySpaceShip;
        [SerializeField] private Transform playerTransform;

        [SerializeField]private float chasingThresholdDistance = 1.0f;

        
        void Update()
        {
            MoveToPlayer();
            enemySpaceShip.Fire();
        }

        private void MoveToPlayer()
        {
            Vector3 enemyPosition = transform.position;
            Vector3 playerPosition = playerTransform.position;
            enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position
            
            Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
            Vector3 directionToTarget = vectorToTarget.normalized;
            Vector3 velocity = directionToTarget * enemySpaceShip.Speed;
                       
            float distanceToTarget = vectorToTarget.magnitude;

            if (distanceToTarget > chasingThresholdDistance)
            {
                transform.Translate(velocity * Time.deltaTime);
            }

        }
        
        
        /*
        private void OnDrawGizmos()
        {
            CollisionDebug();
        }

        private void CollisionDebug()
        {
            if (enemyRenderer != null && playerRenderer != null)
            {
                if (intersectAABB(enemyRenderer.bounds, playerRenderer.bounds))
                {
                    Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.white;
                }
                
                Gizmos.DrawWireCube(enemyRenderer.bounds.center, 2 * enemyRenderer.bounds.extents);
                Gizmos.DrawWireCube(playerRenderer.bounds.center, 2 * playerRenderer.bounds.extents);
            }
        }

        private bool intersectAABB(Bounds a, Bounds b)
        {
            return (a.min.x <= b.max.x && a.max.x >= b.min.x)
                   && (a.min.y <= b.max.y && a.max.y >= b.min.y);
        }
        */
    }    
}

