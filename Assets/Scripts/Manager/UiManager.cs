﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class UiManager : MonoBehaviour
    {
        [SerializeField] public Button startButton;
        [SerializeField] public Button restartButton;
        [SerializeField] public Button nextStageButton;
        [SerializeField] public Button quitButton;
        
        [SerializeField] private RectTransform startDialog;
        [SerializeField] public RectTransform restartDialog;

        [SerializeField] private StageManager stageManager;

        void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(quitButton != null, "quitButton cannot be null");
            
            Debug.Assert(startDialog != null, "startDialog cannot be null");
            Debug.Assert(restartDialog != null, "restartDialog cannot be null");
        }

        public void OpenOrCloseMenuOf(RectTransform target, bool openOrNot)
        {
            target.gameObject.SetActive(openOrNot);
        }
        
        public void OnStartButtonClicked()
        {
            OpenOrCloseMenuOf(startDialog,false);
            stageManager.StartGame();
        }
        
        public void OnRestartButtonClicked()
        {
            stageManager.DestroyRemainingShips();
            OpenOrCloseMenuOf(restartDialog, false);
            stageManager.StartGame();
        }

        public void OnNextStageButtonClicked()
        {
            SceneManager.LoadScene("Stage2");
        }

        public void OnQuiteButtonClicked()
        {
            SceneManager.LoadScene("Stage1");
        }
    }
}