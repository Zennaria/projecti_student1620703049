﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spaceship;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Manager
{
    public class StageManager : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceShip playerSpaceship;
        [SerializeField] private EnemySpaceShip enemySpaceship;

        [SerializeField] private int playerHp;
        [SerializeField] private int playerSpeed;
        [SerializeField] private int enemyHp;
        [SerializeField] private int enemySpeed;

        [SerializeField] private UiManager uiManager;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;

        private void Awake()
        {
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");

            scoreManager.HideScore(true);
            uiManager.OpenOrCloseMenuOf(uiManager.restartDialog, false);
            uiManager.startButton.onClick.AddListener(uiManager.OnStartButtonClicked);
        }
        
        public void StartGame()
        {
            scoreManager.Init();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerHp, playerSpeed);
            spaceship.OnExplode += OnPlayerSpaceshipExploded;
        }

        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemyHp, enemySpeed);
            spaceship.OnExplode += OnEnemySpaceshipExploded;
        }
        
        public void OnPlayerSpaceshipExploded()
        {
            StageEnd();
        }

        public void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(100);
            StageEnd();
        }

        private void StageEnd()
        {
            uiManager.OpenOrCloseMenuOf(uiManager.restartDialog,true);
            OnRestarted?.Invoke();
            
            uiManager.restartButton.onClick.AddListener(uiManager.OnRestartButtonClicked);
            uiManager.nextStageButton.onClick.AddListener(uiManager.OnNextStageButtonClicked);
            uiManager.quitButton.onClick.AddListener(uiManager.OnQuiteButtonClicked);
        }
        
        public void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }
        
    }
}