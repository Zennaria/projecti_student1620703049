﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;
        
        [SerializeField] private StageManager stageManager;
        private int playerScore;
        
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot be null");
        }

        public void Init()
        {
            stageManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score {score}";
            playerScore = score;
            
        }

        private void OnRestarted()
        {
            finalScoreText.text = $"Player Score : {playerScore}"; 
            stageManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        public void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}