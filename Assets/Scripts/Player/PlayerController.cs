﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceShip playerSpaceShip;
        
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;

        private float minX;
        private float maxX;
        private float minY;
        private float maxY;

        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
        }

        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceShip.Fire();
        }
        
        private void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }

            if (obj.canceled)
            {
                movementInput = Vector2.zero;
            }
        }
        
        private void Update()
            {
                Move();
            }
        
        private void Move()
        {
            var inPutVelocity = movementInput * playerSpaceShip.Speed;

            var newPosition = transform.position;
            newPosition.x = transform.position.x + inPutVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + inPutVelocity.y * Time.smoothDeltaTime;
            
            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);

            transform.position = newPosition;
        }

        private void CreateMovementBoundary()
        {
            var mainCamara = Camera.main;
            Debug.Assert(mainCamara != null,"mainCamara cannot be null");

            var spriteRenderer = playerSpaceShip.GetComponent<SpriteRenderer>();
            var offset = spriteRenderer.bounds.size;
            Debug.Assert(spriteRenderer != null,"spriteRenderer cannot be null");
            
            minX = mainCamara.ViewportToWorldPoint(mainCamara.rect.min).x + offset.x/2;
            maxX = mainCamara.ViewportToWorldPoint(mainCamara.rect.max).x - offset.x/2;
            minY = mainCamara.ViewportToWorldPoint(mainCamara.rect.min).y + offset.y/2;
            maxY = mainCamara.ViewportToWorldPoint(mainCamara.rect.max).y - offset.y/2;
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }
    }
}
