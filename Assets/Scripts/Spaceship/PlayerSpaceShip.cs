﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExplode;

        [SerializeField] private AudioClip playerFireSound, playerExplodedSound;
        [SerializeField] private float playerFireSoundVolume, playerExplodedSoundVolume;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null,"defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(playerFireSound != null, "playerFireSound != null");
            Debug.Assert(playerExplodedSound != null, "playerExplodedSound != null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound, transform.position, playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerExplodedSound, transform.position, playerExplodedSoundVolume);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            Debug.Log("you are dead bro!");
            Destroy(gameObject);
            OnExplode?.Invoke();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            Debug.Log("Hit!");
            Explode();
        }
    }
}
