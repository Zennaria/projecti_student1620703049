﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExplode;
        [SerializeField] private double fireRate = 2;
        private float fireCounter = 0;
        
        [SerializeField] private AudioClip enemyFireSound, enemyExplodedSound;
        [SerializeField] private float enemyFireSoundVolume, enemyExplodedSoundVolume;
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
        
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(enemyExplodedSound, transform.position, enemyExplodedSoundVolume);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExplode?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                AudioSource.PlayClipAtPoint(enemyFireSound, transform.position, enemyFireSoundVolume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
        
    }
}